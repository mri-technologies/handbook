# Extravehicular Activity (EVA) Office

## Contracts

- EVA Space Operations Contract (ESOC)
  - Support for the [EMU](https://www.nasa.gov/pdf/188963main_Extravehicular_Mobility_Unit.pdf)
  - Subcontractor to [Collins Aerospace](https://www.collinsaerospace.com/)
- JSC Engineering, Technology and Science (JETS)
  - Support for the [xEMU](https://www.nasa.gov/image-feature/exploration-emu-xemu-development-unit)
  - Subcontractor to [Jacobs](https://www.jacobs.com/)

## Roles

### Project Manager

This role extends the general [Project Manager](../company/organization.md#project-manager) role.

### Lead Frontend Engineer

This role extends the general [Frontend Engineer](../company/organization.md#frontend-engineer) role.

#### Requirements

- Lead day to day assignment of work to Frontend Engineering team
- Demonstrates self-sufficiency and eagerness to improve product quality and UX
- Works well with fellow engineers (Frontend, Backend), the [Project Manager](../company/organization.md#project-manager), and the customer to develop requirements and technical solutions
- Expert level proficiency with VueJS
- Ability to pass on knowledge and experiences to new [Frontend Engineer](../company/organization.md#frontend-engineer) on the team

### Senior Backend Engineer

This role extends the general [Backend Engineer](../company/organization.md#backend-engineer) role

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality
- Works well with fellow engineers (Frontend, Backend), the [Project Manager](../company/organization.md#project-manager), and the customer to develop requirements and technical solutions
- Expert level proficiency with GraphQL / Postgres
- Experience managing and refining large datasets
- Experience developing robust, public-facing APIs

### Frontend Engineer

COSMIC is a Frontend heavy system that is maintained via a large team with varying roles all working together as a single unit to build the Frontend system.

All roles extends the general [Frontend Engineer](../company/organization.md#frontend-engineer) role.

### New Feature Development Engineer

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality
- Ability to read customer requirements and translate to new system features
- Proven ability to create new features that are data dense but simple to understand
- Excellent system-level knowledge

### Quality Assurance Engineer

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality
- Testing and verification of the system prior to production releases
- Documentation of the system in the form of a user guide
- Building automated tests to verify system functionality and integrity
- Experience with creating tests within Jest
- Excellent system-level knowledge

### Operations and Maintenance Engineer

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality
- Responsible for production level issues, system improvements
- Monitors and responds to errors logged in the COSMIC Admin panel and is aware of the current state of the production system

## Applications

### COSMIC

The COSMIC project encompasses all systems and work created and maintained for the ESOC (EVA Space Operations Contract) and xEMU programs.

- **Project Phase**: [Graduated](../company/projects.md#graduated)
- **Data Classification**: Moderate
- **AART ID**: ...
- **NAMS ID**: ...
- **Environments**
  - **Production**: https://eva.nasa.gov/cosmic/
  - **Staging**: https://eva-stage.nasa.gov/cosmic/
- **Team**:
  - **Lead**: [`@mfielden`](https://appdat.jsc.nasa.gov/mfielden), [`@rudism`](https://appdat.jsc.nasa.gov/rudism)
  - **Project Manager**: [`@ttucker1`](https://appdat.jsc.nasa.gov/ttucker1)
  - **SRE**: TBD
