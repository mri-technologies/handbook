# Flight Operations Directorate (FOD)

## Contracts

- Communications, Outreach, Multimedia and Information Technology  (COMIT)
  - Information Technology and Innovation Fund (ITIF) recipient
  - Subcontractor to [MORI Associates](https://moriassociates.com/)

## Applications

### MTS

The Mission Telemetry Services (MTS) project is a funded zero-trust prototype project related to the capture and dissemination of ISS telemetry through a cloud-native ZTA architecture.

- **Project Phase**: [Incubator](../company/projects.md#incubator)
- **Data Classification**: Moderate
- **AART ID**: ...
- **NAMS ID**: ...
- **Environments**
  - **Production**: ...
  - **Staging**: ...
- **Team**:
  - **Lead**: [`@dnic`](https://appdat.jsc.nasa.gov/dnic)
  - **Project Manager**: TBD
  - **SRE**: TBD
