# Projects

This section is organized by customer, each with their own top-level markdown file. For an overview of how we define project lifecycles see [Project Phases](../company/projects.md).
