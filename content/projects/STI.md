# Science and Technical Information (STI)

The NASA Scientific and Technical Information (STI) Program is a critical component in the worldwide activity of scientific and technical aerospace research and development. The NASA STI Program acquires, processes, archives, announces, and disseminates NASA STI and acquires worldwide STI of critical importance to NASA and the Nation.

Collected from U.S. and international sources, STI is organized according to content prior to being added to the [NASA STI Repository (NTRS)](#ntrs). The STI Repository is a world-class collection of STI that includes over **4.3 million metadata records** and over **half a million full-text documents**.

The NASA STI Program uses the [Scientific, Technical and Research Information discoVEry System (STRIVES)](#strives) to standardize STI submission, review and approval across all 10 NASA field centers.

For more information see: https://sti.nasa.gov/what-is-the-nasa-sti-program/

## Contracts

- Communications, Outreach, Multimedia and Information Technology  (COMIT)
  - Subcontractor to [MORI Associates](https://moriassociates.com/)

## Roles

### Project Manager

This role extends the general [Project Manager](../company/organization.md#project-manager) role.

### Senior Backend Engineer

This role extends the general [Backend Engineer](../company/organization.md#backend-engineer) role.

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality
- Works well with fellow engineers (Frontend, Backend), the [Project Manager](../company/organization.md#project-manager), and the customer to develop requirements and technical solutions
- Expert level proficiency with Typescript/Node and/or C#
- Experience managing and refining large (terabyte-scale) datasets: aggregation, curation, enrichment
- Experience developing robust, public-facing APIs
- Experience with Postgres and Elasticsearch (preferred)
- Experience with OpenAPI schema generation (preferred)
- Domain knowledge related to the scientific publication process and research access (preferred)

### Senior Frontend Engineer

This role extends the general [Frontend Engineer](../company/organization.md#frontend-engineer) role.

#### Requirements

- Demonstrates self-sufficiency and eagerness to improve product quality and UX
- Works well with fellow engineers (Frontend, Backend), the [Project Manager](../company/organization.md#project-manager), and the customer to develop requirements and technical solutions
- Expert level proficiency with Typescript and Angular
- Experience with reactive state management (preferred)
- Experience with server-side rendering (SSR) tooling like Angular Universal (preferred)
- Experience with static site generators like Hugo (preferred)

## Applications

### STRIVES

- **Project Phase**: [Graduated](../company/projects.md#graduated)
- **Data Classification**: Moderate / ITAR
- **AART ID**: ...
- **NAMS ID**: ...
- **Environments**:
  - **Production**: https://strives.nasa.gov/
  - **Staging**: https://strives.staging.sti.appdat.jsc.nasa.gov/
- **Team**
  - **Lead**: [`@marshall007`](https://appdat.jsc.nasa.gov/marshall007)
  - **Project Manager**: [`@lstone`](https://appdat.jsc.nasa.gov/lstone)
  - **SRE**: [`@adamn.stracener`](https://appdat.jsc.nasa.gov/adam.stracener)

### NTRS

- **Project Phase**: [Graduated](../company/projects.md#graduated)
- **Data Classification**: Moderate / ITAR
- **AART ID**: ...
- **NAMS ID**: ...
- **Environments**
  - **Production**: https://ntrs.nasa.gov/
  - **Staging**: https://ntrs.staging.sti.appdat.jsc.nasa.gov/
- **Team**
  - **Lead**: [`@marshall007`](https://appdat.jsc.nasa.gov/marshall007)
  - **Project Manager**: [`@lstone`](https://appdat.jsc.nasa.gov/lstone)
  - **SRE**: [`@adamn.stracener`](https://appdat.jsc.nasa.gov/adam.stracener)

### STI Static Site

- **Project Phase**: [Graduated](../company/projects.md#graduated)
- **Project Manager**: [`@lstone`](https://appdat.jsc.nasa.gov/lstone)
- **Environments**
  - **Production**: https://sti.nasa.gov/
  - **Staging**: https://static-site.staging.sti.appdat.jsc.nasa.gov/
- **Team**
  - **Lead**: [`@nkeeney`](https://appdat.jsc.nasa.gov/nkeeney)
  - **Project Manager**: [`@lstone`](https://appdat.jsc.nasa.gov/lstone)
  - **SRE**: [`@adamn.stracener`](https://appdat.jsc.nasa.gov/adam.stracener)

### DOIMS

- **Project Phase**: [Incubator](../company/projects.md#incubator)
- **Data Classification**: Low
- **AART ID**: ...
- **NAMS ID**: https://idmax.nasa.gov/nams/asset/255044
- **Environments**
  - **Production**: https://doi.data.nasa.gov/
  - **Staging**: https://doims.staging.appdat.jsc.nasa.gov/
- **Team**
  - **Lead**: [`@marshall007`](https://appdat.jsc.nasa.gov/marshall007)
  - **Project Manager**: [`@lstone`](https://appdat.jsc.nasa.gov/lstone)
  - **SRE**: [`@adamn.stracener`](https://appdat.jsc.nasa.gov/adam.stracener)
