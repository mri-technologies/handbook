# Communication

Having well-formed and well-understood communication practices is vital to all things we accomplish as a team, as well as how we feel about each other and our jobs.  Having respectful and kind communication with one another is a requirement for working on this team. We must encourage and promote the free exchange of ideas within a collaborative environment that feels safe and inviting to all team members.  What that feels like between each of is never exactly the same, but there are core tenants to the values of our organization with respect to how we communicate with one another.

## Core Tenants

- Words Matter - Respect, Respect, Respect
- Be kind, we are all here working for the same goal
- Encourage Ideas - never shut people out from giving their thoughts, encourage it
- Never be afraid to ask a question, respect and praise those who do
- Pride yourself in following up on questions from others. If you don’t know, don’t guess, own it, say you don’t know. Find out and share with the group (preferably documented in Gitlab)
- Lead by example, critical for the management team to be the example in all things like these. Even when its hard, we have to listen, guide, care for and grow others on the team.
- Respect others time - be on time for meetings, don’t drag on meetings too long, respect others family situations, etc

## Meetings

### CIO Core Meetings

Our core meetings are centered around our nominal 2-week iteration cycles, our goal with meetings is to keep them as minimal as possible, while maintaining the awareness and understanding everyone needs. Below are our current team level meetings, individual project managers or project working units will have their own meeting schedules and objectives. Those meetings however need to not conflict with the following core meetings.

#### Team Meetings

1. Weekly Catch up: Weekly, Friday 12-1
    - Casual team wide catch up
    - Demos encouraged
    - Is not a status meeting

#### Iteration Meetings

1. Iteration Kickoff: Week 1, Monday
    - Per project
    - Should include the customer and external stakeholders when able
    - Attendees:
        - Project Managers, Leads
        - CTO or Deputy CIO or CIO depending on project
2. Iteration Planning: Week 1, Friday
    - Per project
    - Internal team, looking at next immediate iteration
    - Run with a screen share and real-time input into project Epics, Iterations, Issues.
        - Issues may need to be broken down or information expanded on to be ready for assignment.
    - Attendees:
        - Project Managers, Leads
        - CTO or Deputy CIO or CIO depending on project
3. Iteration Closeout: Week 2, Thursday
    - Per project
    - Does not need to include customers or external stakeholders unless specifically requested  
    - Attendees:
        - Project Managers, Leads
        - CTO or Deputy CIO or CIO depending on project
    - Close out a sprint leaving Friday open for the team to have a natural breather day

#### Strategic & Leadership Meetings

1. Bi-weekly Strategy Meeting, Iteration Week 2, Monday
    - High level contracts and project status review
    - Attendees:
        - CIO, Deputy CIO, CTO
2. Bi-weekly Management Meeting, Iteration week 2, Friday, 11-12
    - High level project management meeting
    - Attendees:
        - CIO, Deputy CIO, CTO
        - Project Managers
3. Quarterly Team Tag Strategy Meeting
    - Team level strategy
    - Focus on future direction
    - Attendees:
        - CIO, Deputy CIO, CTO
        - Project Managers
        - SREs, Cloud-native Engineers
        - Engineering Leads

## Team Communication Tools

Below is the list of team preferred communication tools and their usage.

## Gitlab

As part of our core business, the CIO division operates a self-hosted Gitlab Ultimate instance as part of the core platform service offering provided to NASA.  The team utilizes this self-hosted Gitlab Ultimate for all persistent, project communications via the various project management structures provided by Gitlab (Epics, issues, etc...).

This Gitlab instance is considered the "single source of truth" for all MRI Dev Team project documentation and communication.  

### Slack

MRI Technologies provides an enterprise Slack workspace for the CIO Division. The team utilizes Slack for all real-time messaging and video conferencing communication.  Project channels are created along with any other public or private channels the team would like to have.  

Within Slack all team, community, and project channels are kept "public" to the entire team to encourage cross functional awareness.  Sensitive conversations should be primarily had via voice/video calls or in cases where that is not possible, via Slack direct message.

All Slack communication should adhere to our [Core Tenants](#core-tenants).  Communication within team and project channels should be remain professional and focused within the scope of the channel.

#### Message Retention

Slack is configured to delete all messages and files after 30 days.  It is in the team's best interest to not allow long term storage of communications and instead focus information that needs to be retained regarding our work products or process to Gitlab.  

## Customer and Partner Communication

Many of the CIO Division roles require team members to directly communicate with our customers and partners via a variety of different communication platforms. The following is preferred communication methods for these cases in ranked order by team preference:

1. Work with customers directly in Gitlab when possible.
2. Work real-time communication needs via NASA Teams when possible.
3. Utilize NASA email when customer or partners are not yet incorporated into Gitlab or NASA Teams.
