# MRI Technologies CIO Division

MRI Technologies has been building high quality, innovative custom software solutions since 1988.  Primarily focused on building aerospace software for NASA, MRI Technologies has built a strong reputation and past performance portfolio for delivering high impact solutions throughout NASA.  

From supporting the Space Shuttle and International Space Station programs, through building cloud-native distributed systems for the next generation space suit programs, into now supporting multiple NASA Information Technology, Science, and Human Exploration divisions across all NASA centers with an industry leading software-factory solution; MRI Technologies will continue to help drive NASA missions forward with critical software systems.

Within MRI, the CIO division operates as an independent, and largely self managed team.  This is due to the highly specialized and technical nature of the work being done by the MRI CIO division.  Working closely with the MRI President and Vice President, the CIO division has the freedom to pursue and execute software projects for NASA that align best with the strategic goals and specialties of the team.  

## Mission

The mission of MRI Technologies CIO Division is to shape and accelerate humanity's ambitions with regard to space exploration through the [careful and well thought-out](values.md#thoughtful) application of open source software and cloud-native technologies.

We consistently undercut and outperform other contractors, providing our customers with the best-in-class software solutions that ambitious federal agencies, like NASA, deserve. Doing so through [Innovation](values.md#inovative) and [Collaboration](values.md#collaborative).

## Vision

MRI Technologies CIO Division believes in the importance of NASA, and all space industry pursuits, to not just the United States, but to all humanity both living and those generations to come.

We believe that a small business; and its dedicated team of passionate, [kind](values.md#kind) and talented people can not only make significant contributions to mankind's space endeavors, but is a key and necessary driver of innovation required for such endeavors.

## Working for NASA

For people unfamiliar with how NASA as a government agency operates, it can be a bit confusing how NASA works with companies like MRI Technologies.  MRI Technologies supports NASA as a [government contractor](https://www.sba.gov/business-guide/grow-your-business/become-federal-contractor) working with NASA civil servants and other NASA contractor partners such as [Collins Aerospace](https://www.collinsaerospace.com/), [Jacobs](https://www.jacobs.com/), [MORI Associates](https://moriassociates.com/), and [Booz Allen](https://www.boozallen.com/).

As a member of several different NASA teams across various NASA contracts, MRI Technologies works directly with NASA to define,document, and build technology and software solutions that will help NASA achieve it's mission.  MRI Technologies CIO Division personnel are badged directly with NASA and work side by side with federal civil servants as a part of the larger, and all encompassing, NASA team.
