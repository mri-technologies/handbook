# MRI Technologies CIO Values

As the CIO division we strive to uphold the following core values.

## Thoughtful

We choose to be thoughtful and intentional with the solutions we present, always considering the future, and always designing extensible and well structured and documented software solutions.  We understand that building technically challenging solutions need thoughtful architects, engineers, and designers.  We understand that working with NASA requires a well formed and thoughtful approach.
  
## Innovative

We maintain an understanding on where technology is moving by following, supporting, and contributing to correlative open source software communities. With this understanding we continually build systems that drive digital transformation through innovation. We utilize what already exists to solve the same core technical functions and leveraging open source; to produce robust forward thinking software solutions.

## Collaborative

We seek out opportunities to work with other teams supporting NASA.  We view the shared goals of the agency above the financial goals of our company.  Through this we always look to provide NASA with the best possible solution, working with both civil servants and other NASA contractors; with a spirit of being one united team.

## Kind

We chose to always default to kindness when working with our teammates, customers, or anyone else we interact with.  We disagree and push each other, and NASA, towards a high standard of software solutions; but we always do so with a kind approach.
