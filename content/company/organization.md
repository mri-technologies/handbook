# Organization

The MRI Technologies CIO Division structure has changed as the team has grown and will continue to do as we evolve as an organization, starting at the CIO level down.

```mermaid
flowchart TB
    %% LABELS

    cio[CIO]
    dcio[Deputy CIO]
    cso[CSO]

    operations_engineers[Operations Engineers]
    operations_lead[Operations Lead]
    security_officers[Security Officers]

    project_managers[Project Managers]
    lead_engineer[Lead Engineer]
    engineers[Engineers]

    sres[SREs]
    cloud_native_engineers[Cloud-Native Engineers]

    graduated_projects{{Graduated Projects}}
    incubator_projects{{Incubator Projects}}
    platform_services{{Platform Services}}

    %% ORG CHART

    cio-->sres
    cio-->cloud_native_engineers
    cio-->dcio
    cio-->operations_lead
    cio-->cso
    

    subgraph customers[Customers]
        subgraph customers_team[Team]
            dcio-->project_managers
            dcio-->lead_engineer
            dcio-->engineers
        end
        subgraph customer_team_projects[Projects]
            project_managers-. Requirements .->graduated_projects
            lead_engineer-. Develop/Support .->graduated_projects
            engineers-. Support .->graduated_projects
        end
    end

    subgraph operations[Operations]
        subgraph operations_team[Team]
            operations_lead-->operations_engineers
            engineers-. Support .->graduated_projects
        end
    end

    subgraph security[Security]
        subgraph security_team[Team]
            cso-->security_officers
        end
    end

    subgraph platform[Platform]
        subgraph platform_team[Team]
            sres
            cloud_native_engineers
        end
        subgraph platform_team_projects[Projects]
            cloud_native_engineers-. Develop/Support .->incubator_projects
            sres-. Support .->platform_services
            sres-. Support .->incubator_projects
        end
    end
```

## Job Tiers

CIO division job tiers aid in mapping a role to a compensation level.  Having a defined mapping helps the team in the following ways:

1. Allows us to define clear expectations for team members across various roles.
1. Improves our ability to accurately estimate cost in response to government requests.
1. Provides transparency with respect to compensation around the team.

| Tier | Management | Engineering
|----- | ---------- | -----------
| 4 | [CIO](#chief-information-officer-cio), [Deputy CIO](#deputy-cio), [CSO](#chief-security-officer) |[Operations Lead](#operations-lead) |
| 3 | [Project Manager](#project-manager) | [SRE](#system-reliability-engineers-sre), [Cloud-Native Engineer](#cloud-native-engineer), [Lead Engineer](#lead-engineer), [Operations Engineer](#operations-engineer)
| 2 | - | [Senior Backend Engineer](#backend-engineer), [Senior Frontend Engineer](#frontend-engineer), [Senior Security Officers](#security-officers)
| 1 | - | [Intermediate Frontend Engineer](#frontend-engineer), [Intermediate Backend Engineer](#backend-engineer), [Intermediate Security Officers](#security-officers)

### Expectations

In conjunction with the [Job Tiers](#job-tiers), Expectations define the things we need team members to contribute
towards our shared efforts. These help everyone understand the expectations for a given Job Tier, the goal of which is
to provide clear paths for personal growth up into high Job Tiers.

| [Job Tier](#job-tier) | Expectations |
| -- | ------------------------- |
| 1  | Works Issues assigned to them.  Understands the iteration objectives assigned to them.
| 2  | Works Issues assigned to them. Understands the iteration objectives assigned to them.  Understands the Epic(s) the iteration is focused on. Helps create issues for themselves and some for others.
| 3  | Works directly with the customer on current projects. Helps plan future projects. Helps creates Issues for themselves and for others.  Helps plan and create the iteration.  Helps plan and create the Epic. Works issues assigned to them.
| 4  | Works directly with the customer on current and future projects. Establishes Projects. Creates Milestones.  Creates and plans the Epic. Creates and plans the iteration. |

## Management

Management is responsible for leading the organization by working together to develop new projects with a
budget and schedule that positions the project for success.  The management team is always looking for new technical
opportunities for the team to grow and learn from.  Management is also responsible for maintaining the quality of
culture, work-life balance, and fun; for the team as a whole.

### Chief Information Officer (CIO)

- **Job Tier**: 4
- **Team Member**: [`@collin.j.estes`](https://appdat.jsc.nasa.gov/collin.j.estes)

#### Responsibilities

- Setting overall strategy and communicate it with the team and our partners
- Maintaining and growing the core business
- Helping the team grow their skills and experience
- Hiring great people
- Working with NASA directly to provide the best possible support
- Working with the [Deputy CIO](#deputy-cio) to plan projects
- Working with the [CSO](#chief-security-officer-cso) and [Deputy CSO](#deputy-cso) to continuously monitor the overall security posture of our team and our systems
- Understanding the NASA political landscape with respect to the organization's within the agency we do business with and those we would like to
- Overseeing all day to day operations
- Maintaining awareness of the broader commercial and public sectors applicable to our business
- Drafting responses to requests, and execute contracts
- Working to continuously automate and streamline team processes

### Deputy CIO

- **Job Tier**: 4
- **Team Member**: [`@ttucker1`](https://appdat.jsc.nasa.gov/ttucker1)

#### Responsibilities

- Managing the [Customer Team](#customer-engineers) and its day to day operations
- Working with NASA directly to provide the best possible support
- Working with the [CIO](#chief-information-officer-cio) to plan projects
- Understanding the NASA political landscape with respect to the organization's with the agency we do business with and those we would like to
- Maintaining awareness of the broader commercial and public sectors applicable to our business
- Drafting responses to requests, and execute contracts
- Working to continuously automate and streamline team processes

### Chief Security Officer (CSO)

- **Job Tier**: 4
- **Team Member**: [`@collin.j.estes`](https://appdat.jsc.nasa.gov/collin.j.estes) _(Acting)_

#### Responsibilities

- Overseeing all aspects of the security operations of the team, as well as all platform customers
- Serving as the Information System Security Officer (ISSO) on all MRI managed NASA Systems
- Creation of all platform "Customer Responsibility Matrix" (CRMs) documentation
- Understanding and maintaining NASA compliance and the platform "Authority to Operate" (ATO)
- All security related communications, presentations and relevant NASA touch points
- Provides security requirements to the [CIO](#chief-information-officer-cio),[Deputy CIO](#deputy-cio), and [Operations Lead](#operations-lead)

### Operations Lead

- **Job Tier**: 4
- **Team Member**: [`@adam.stracener`](https://appdat.jsc.nasa.gov/adam.stracener)

#### Responsibilities

- Overseeing the [Operations Team](#operations-engineers) and its day to day operations
- Responsible for all day to day operational duties for supporting the [Customer Teams](#customer-engineers) and [Platform Teams](#Platform-engineers).
- Sets operational priorities working closing with the [CIO](#chief-information-officer-cio), [CSO](#chief-security-officer-cso), and [Deputy CIO](#deputy-cio)
- Responsible for the configuration and management of all Cloud Services utilized by the MRI Dev Team as a corporation, or utilized with the Platform on NASA owned cloud service infrastructure.

### Project Manager

- **Job Tier**: 3

#### Responsibilities

- Developing and managing detailed project plans
- Leading teams by prioritizing work schedule to achieve on time project completion
- Providing consistent project updates (internally and externally)
- Liaison between customer and [Engineers](#customer-engineers)
- Distilling customer requirement into actionable and achievable work items
- Working closely with project technical lead to ensure success and quality in all deliverables

## Platform Engineers

Platform Engineers report directly to the [CIO](#chief-information-officer-cio) and are responsible for architecting and developing [Incubator Projects](projects.md#incubator) and maintaining shared [Platform Services](projects.md#platform-services).

### Requirements for all Platform Engineers

- Experience with Kubernetes in production
- Demonstrable success adopting Service Mesh in production (preferred, but not required)
- Proficiency with Terraform (or equivalent) for infrastructure automation tooling
- Extensive Linux experience (familiarity with Windows also preferred, but not required)
- Expert in at least one programming language (Go is preferred)
- Solution-oriented mindset, accepts feedback with enthusiasm
- Effectively communicates with team members: clear status updates to direct report, regularly establishes unanimous decisions with peers
- Ability to thrive in a fully remote organization
- Self-motivated and self-managing, with strong organizational skills
- Ability to use GitLab (preferably very proficient)

### System Reliability Engineer (SRE)

- **Job Tier**: 3

#### Requirements

- [Requirements for all Platform Engineers](#requirements-for-all-platform-engineers)
- Security-oriented mindset, always identifying opportunities to rigorously enforce [least privileged access](https://csrc.nist.gov/glossary/term/Principle_of_Least_Privilege)
- Experience in developing robust and reusable deployment pipelines for cloud-native workloads
- Expert level proficiency with monitoring and log aggregation tools (Elastic Stack preferred)
- Proficiency across a variety of technical stacks, ability to debug customers' production workloads that we don't maintain
- Experience designing, building, and maintaining scalable core platform infrastructure
- Considers manual action a last resort, documents manual action into repeatable process, automates repeatable processes

#### Responsibilities

- Developing and maintaining shared [Platform Services](projects.md#platform-services)
- Empowering [Customer Engineers](#customer-engineers) to be self-sufficient through documentation and tooling
- Identifying good opportunities to streamline and automate process
- Maintaining documentation and runbooks, promoting knowledge sharing
- Proactively identifying risks, providing solutions for how to manage (stay ahead of) them

### Cloud-Native Engineer

- **Job Tier**: 3

#### Requirements

- [Requirements for all Platform Engineers](#requirements-for-all-platform-engineers)
- Experience architecting and building cloud-native applications
- Demonstrable success transforming and migrating legacy application stacks to a cloud-native architecture
- Demonstrable success and confidence in owning the full project lifecycle
- Good instincts for identifying opportunities for abstraction
- Balances individual project development tasks at hand with overall platform and company goals

#### Responsibilities

- Developing [Incubator Projects](projects.md#incubator)
- Supporting [SREs](#system-reliability-engineer-sre) in the development and maintenance of [Platform Services](projects.md#platform-services)
- Working with the [CIO](#chief-information-officer-cio) to shepherd projects through the [Graduation Process](projects.md#graduated)
- Developing best practices for cloud-native application development and promoting them within the organization
- Writing quality code, providing quality and engaged code reviews for peers
- Identifying opportunities to abstract [Prospective Project](projects.md#prospective) requirements and develop Enterprise-grade, multi-tenant [Platform Services](projects.md#platform-services)

## Customer Engineers

Customer Engineers report directly to the [Deputy CIO](#deputy-cio) and are primarily responsible for working with their designated [Project Manager](#project-manager) to develop and maintain [Graduated Customer Projects](projects.md#customer-projects).

### Requirements for all Customer Engineers

- Significant professional experience with the language required by the [project](../projects/_index.md)
- Professional experience with any other technologies that may be required by the [project](../projects/_index.md)
- Solution-oriented mindset, accepts feedback with enthusiasm
- Effectively communicates with team members: clear status updates to direct report, regularly establishes unanimous decisions with peers
- Ability to thrive in a fully remote organization
- Self-motivated and self-managing, with strong organizational skills
- Ability to use Git and GitLab (preferably very proficient)

### Lead Engineer

There is no explicit "Lead" engineering role within the organization. Leads are appointed on a per project basis, with responsibilities tailored to the project's needs.

Information on specific project roles and their designated leads can be found on each [project](../projects/_index.md) page.

Any engineer that demonstrates an aptitude for leadership of their peers and/or managing customer relationships may be appointed as a "Lead", but on larger projects appointees are generally limited to **Senior** ([T2](#job-tiers)) and **Staff** ([T3](#job-tiers)) engineers.

### Backend Engineer

- **Job Tier**
  1. **Intermediate**
  2. **Senior**
  3. **Staff**

#### Requirements

- [Requirements for all Customer Engineers](#requirements-for-all-customer-engineers)
- Demonstrable ability to reason about and communicate complex technical, architectural, and/or organizational problems
- Experience with Docker in both a local development and deployment context
- Experience developing and maintaining robust CI pipelines

#### Responsibilities

- Collaborate with the [Product Manager](#project-manager) and fellow engineers ([Frontend](#frontend-engineer), UX, etc.) to develop and maintain [Graduated Customer Projects](projects.md#customer-projects)
- Develop a deep understanding of the relevant [project](../projects/_index.md) domain
- Propose and implement technical solutions to highly domain-specific problems
- Continuously advocate for improvements to product and code quality, identify and eliminate technical debt

### Frontend Engineer

- **Job Tier**:
  1. **Intermediate**
  2. **Senior**
  3. **Staff**

#### Requirements

- [Requirements for all Customer Engineers](#requirements-for-all-customer-engineers)
- Strong understanding of the web browser environment
- Professional experience with Typescript (preferred, but not explicitly required)
- Professional experience with the frontend framework (Vue.js, Angular, React, etc) required by the [project](../projects/_index.md)
- Experience with Docker in a local development context

#### Responsibilities

- Collaborate with the [Product Manager](#project-manager) and fellow engineers ([Backend](#backend-engineer), UX, etc.) to develop and maintain [Graduated Customer Projects](projects.md#customer-projects)
- Develop a deep understanding of the relevant [project](../projects/_index.md) domain
- Propose and implement robust UIs with quality UX to address highly domain-specific problems
- Continuously advocate for improvements to product and code quality, identify and eliminate technical debt

## Operations Engineers

Operations Engineers report directly to the [Operations Lead](#operations-lead) are primarily responsible for overseeing all the day to day operational health and security aspects of both the Platform and Customer projects.

- **Job Tier**: 3

## Requirements

- Security-oriented mindset, always identifying opportunities to rigorously enforce [least privileged access](https://csrc.nist.gov/glossary/term/Principle_of_Least_Privilege)
- Significant professional experience with at least one of the major cloud service providers: AWS, GCP, Azure.
- Significant professional experience working with "Infrastructure-as-Code" solutions
- Experience monitoring production applications in real time
- Experience working with hybrid-cloud environments
- Significant professional experience working with Linux based server technologies

### Security Officers

Security Officers report directly to the [CSO](#chief-security-officer) and are responsible for the day to day security operations.

- **Job Tier**:
  1. **Intermediate**
  2. **Senior**

#### Responsibilities

- Performing day to day security operations
- Provide system security plan documentation support
- Enforcing a policy of [least privileged access](https://csrc.nist.gov/glossary/term/Principle_of_Least_Privilege) throughout all aspects of the business
- Continuous monitoring of all technical components used by the team, as well as all platform customers
- Responsible for reviewing all security incidents, remediation processes/timelines, and offboarding of non-compliant workloads
