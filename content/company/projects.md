# CIO Project Phases

```mermaid
graph LR
    prospective{{Prospective Projects}}
    incubator{{Incubator Projects}}
    platform{{Platform Services}}
    graduated{{Graduated Projects}}
    eol{{End-of-Life}}

    graduation{"Graduation"}
    multi_tenant[/"Multiple demand signals?"/]

    prospective-->incubator
    incubator-->graduation
    graduation-->multi_tenant
    multi_tenant-. Yes .->platform
    multi_tenant-. No .->graduated

    graduated-. Demand .->graduation
    graduated-. Lapse in Funding .->eol
```

## Prospective

- **Lead:** [CIO](organization.md#chief-information-officer-cio)
- **Project Manager:** [CIO](organization.md#chief-information-officer-cio) and/or [CTO](organization.md#chief-technology-officer-cto)

We are always looking to grow our business, solve new challenges, and streamline NASA's operations in every way we can. Such opportunities present themselves in several forms, namely:

1. NASA program with a problem they have no existing technical solution for (i.e. new application development)
1. NASA program with an existing technical solution they wish to migrate to the cloud and potentially streamline
1. Identification of cross-cutting problems and/or solutions that could impact existing or potential NASA program customers

There is a good chance NASA is the largest and most complex organization you have ever worked with. You will be presented with exciting ideas and challenges on nearly a daily basis. It can be tempting to involve yourself in meetings with NASA civil servants and even other contractors to pursue these opportunities, but it is important to remember you are accountable to your team and direct manager first and foremost.

So, while all team members are encouraged to identify new opportunities, you _must_ route any solicitations or project ideas through your direct manager or the [CIO](organization.md#chief-information-officer-cio) directly before any work is started. A key [responsibility of the CIO](organization.md#chief-information-officer-cio) is maintaining relationships with NASA programs, identifying cross-cutting concerns/requirements, and proactively identifying opportunities for engagement.

Projects in this phase require us to: establish a funding vehicle, prioritize work, identify opportunities for consolidation, and abstract requirements into [Platform Services](#platform-services).  To do so we need the broad understanding of the CIO, working in conjunction with the CTO, to work with an individual team member from the initial engagement forward.  When individual team members engage these opportunities without the CIO, the impact of the implementation is reduced, the prioritization of the effort is not understood or communicated, and ultimately it makes us less efficient as a team.

## Incubator

- **Lead:** [Cloud-Native Engineer](organization.md#cloud-native-engineer)
- **Project Manager:** [Cloud-Native Engineer](organization.md#cloud-native-engineer)

Generally speaking, before any prototyping begins:

1. Due diligence is performed by the [CIO](organization.md#chief-information-officer-cio)
1. Project planning is done by the [CIO](organization.md#chief-information-officer-cio) in conjunction with the [CTO](organization.md#chief-technology-officer-cto)
1. [CTO](organization.md#chief-technology-officer-cto) in conjunction with the [CIO](organization.md#chief-information-officer-cio) determines whether the scope of the MVP should target [a single-tenant](#customer-projects) or [a multi-tenant service offering](#platform-services)
1. [CTO](organization.md#chief-technology-officer-cto) names one or more [Cloud-Native Engineers](organization.md#cloud-native-engineer) responsible for the project
1. The resulting [Incubator Project](#incubator) is documented in the ["Projects" section of the handbook](../projects/_index.md)

The purpose of the "incubation" project stage is to move fast and iterate quickly towards an MVP (minimum viable product) that achieves the project deliverables defined by the customer.

We achieve this in part by having very little management overhead at this stage. As a result, we expect [Cloud-Native Engineers](organization.md#cloud-native-engineer) to be a ["manager of one"](https://signalvnoise.com/posts/1430-hire-managers-of-one).

> **Note:** as an all-remote organization, we want _everyone_ to be a "manager of one", but it is a strict requirement for [Platform Engineers](organization.md#platform-engineers).

Cloud-Native Engineers have the ultimate say on all technical decisions for incubator projects they are responsible for. These decisions can be contested by management (namely the [CTO](organization.md#chief-technology-officer-cto)), but not in a way that blocks iterative development towards an MVP.

## Graduated

After an [Incubator Project](#incubator) has reached an MVP state that meets the customer requirements, another formal process takes place to transition it into an operational state (what NASA generally refers to as "Operations and Maintenance" or O&M). At a high level, this process involves:

1. Verifying the project has completed its initial development and testing phase
1. Verifying "feature completeness" as originally defined by the deliverables from the customer's perspective
1. Includes an appropriate level of documentation, including: end-user guide(s), developer/contribution docs, running locally, deploying to production, etc
1. SSP (or other required FedRAMP documentation) has been completed
1. NASA AART entry has been created
1. NAMS workflow has been established and application is integrated with NASA Launchpad via SAML or APPDAT Keycloak (when authz is required)
1. Corresponding entry for the relevant [Incubator Project](#incubator) is updated in the ["Projects" section of the handbook](../projects/_index.md) to reflect its [Graduated Project](#graduated) status and new stakeholders

Once all these requirements have been met, the project is considered ["graduated"](#graduated) and, depending on its scope, ownership may be transferred internally from the [Platform Team](organization.md#platform-engineers) to the [Customer Team](organization.md#customer-engineers).

### Customer Projects

- **Lead:** [Deputy CIO](organization.md#deputy-cio) and/or [Lead Customer Engineer](organization.md#customer-engineers)
- **Project Manager:** [Customer Project Manager](organization.md#project-manager)

Projects developed for and/or maintained on behalf of a single customer fall under this category.

For such projects, in addition to the process defined above:

1. [Deputy CIO](organization.md#deputy-cio) names a [Project Manager](organization.md#project-manager), or if a dedicated PM resource is not available/needed may appoint himself
1. [Deputy CIO](organization.md#deputy-cio) names one or more [Customer Engineers](organization.md#customer-engineers) to support the project
1. [Deputy CIO](organization.md#deputy-cio) coordinates with the [CTO](organization.md#chief-technology-officer-cto) to finalize ownership transfer and ensure the process is as seamless as possible from the customer's perspective

### Platform Services

- **Lead:** [SRE](organization.md#system-reliability-engineer-sre)
- **Project Manager:** [CTO](organization.md#chief-technology-officer-cto)

Projects that address cross-cutting concerns based on multiple demand signals are specified and implemented as multi-tenant platform services in their [Incubator](#incubator) stage. These projects either support multiple NASA programs/customers directly or are otherwise standalone services that can be consumed/used by anyone at NASA in a (relatively) self-service fashion.

For such projects, in addition to the process defined above:

1. [CTO](organization.md#chief-technology-officer-cto) becomes the effective [Project Manager](organization.md#project-manager)
1. [CTO](organization.md#chief-technology-officer-cto) names one or more [SREs](organization.md#system-reliability-engineer-sre) responsible for long-term operational aspects of the project

## End-of-Life (TBD)

This aspect of a project's lifecycle needs to be fleshed out and we could potentially even have APPDAT services that support this directly. Unlike a typical enterprise, the federal government is generally obligated to continue providing taxpayer funded services, research, and particularly data to the general public indefinitely. This requirement doesn't change just because a particular NASA program has a lapse in funding. Such programs have to find a way to limit their operational costs while still providing public access (i.e. putting their dynamic site into a static archival state).
