# MRI Technologies CIO Handbook

This handbook is a place where, together, we can create a better, more transparent framework for our team. A place where we can collaborate on ideas openly as a whole team, instead of only communicating with those we feel most comfortable with at any given point in time. By establishing clear structures, roles, and processes; we can ensure that we provide each other with the support and accountability we need.  These structures, roles, and processes will need to be continuously changed, updated, expanded, and modernized. To accomplish this we need to make the creative process of building and editing this handbook an open and collaborative effort that encompasses the design of our team.

We also need to apply this philosophy of growth and adaptation not only to our projects and deliverables, but more importantly in ourselves and the people we get to work with programming and building things for NASA. Our culture, communication and human connections we build on this team matter, and we should strive to be modernizing these elements of our team as much as we modernize our technical implementations.  

We need to always be constantly challenging ourselves and our [vision](content/company/_index.md#vision) as we work to accomplish our [mission](content/company/_index.md#mission). We have the power to write who we are and what we do.  

Leave our mark.

## Why we built this handbook

1. Stop and thoughtfully consider our mission and goals as an organization and set an expectation that we will put this level of thought into our organization as a continuous process going forward.
1. Manage company process and values the same way we manage infrastructure: as code everyone is free to contribute to and modify.
1. Make our organization appealing to the kinds of people we want to hire.
1. Make on-boarding new employees significantly easier, less time-consuming, and provides them easy opportunities for contribution on day-one
1. Communicating changes to expectations or company processes is clear and easy when you can just point to a diff/MR
1. To have better source of truth for HR and corporate information, such as health plans, holiday time, time-sheets, reimbursement, travel, etc...
