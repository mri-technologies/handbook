# Contributing

All MRI Technologies team members are encouraged to help contribute to this living handbook.  Follow the process below

## How to Contribute

1. [Open an Issue](https://gitlab.com/mri-technologies/handbook/-/issues/new?issue) in this repository.
    - Select the right Issue Template: Add_Page, Add_Section, Edit_Section
    - Describe your idea
    - Soliciate feedback from other team members and management
2. Create a branch and "Draft" Merge Request
3. Make updates and mark MR ready for review
    - Pipeline should pass
    - Updates should be considered finalized by the contributor
    - Code Owners will determine approvals


## Lint and Spellchecking

We use [`ci/markdown-tools`](https://appdat.jsc.nasa.gov/ci/markdown-tools) to maintain a standardized format and ensure spelling errors are caught.

If you need to add a word to the spellchecker's dictionary my modifying the [dictionary.txt](./dictionary.txt) file.

You can also run the linter and spellchecker locally like so:

```sh
docker run \
  -v /path/to/project:/builds \
  registry.appdat.jsc.nasa.gov/ci/markdown-tools/master:latest \
  lint --fix
```
