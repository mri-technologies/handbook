# Add Section to existing page
<!-- briefly describe the change you are suggesting --> 

The handbook section that should be added is: example-page.md

The section should:

### Who is the primary audience? Be specific.

(Example: Potential developers vs managers vs customers)

/label type::add-section
