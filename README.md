# MRI Technologies CIO Handbook

This handbook serves as the "single source of truth" for the MRI Technologies CIO division.

There are two sections:

1. [Company](./content/company/_index.md)
2. [Projects](./content/projects/_index.md)

All team members are encouraged to help update and expand this handbook, see [`CONTRIBUTING`](./CONTRIBUTING.md) for details.
